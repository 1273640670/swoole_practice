<?php

$a = 0;
for ($i = 0; $i < 10; $i++) {
    $process = new Swoole\Process(function () use ($i, &$a) {
        $a++;
        //子进程空间执行业务逻辑
        sleep(1);
        //获取进程pid
        echo getmypid() . '正在执行任务' . $i . PHP_EOL;
    });
    $process->start();  //自动进程，创建进程
}

//pcntl-php另一个扩展
//pstree -ap|grep php    查看树形结构进程

//回收子进程-防止产生僵尸进程
//阻塞回收形式
while ($ret = Swoole\Process::wait(true)) {
    var_dump($ret);
}
echo $a . PHP_EOL;    //$a没有变化，每个进程的内存地址都是独立复制出来的，不共用


//worker进程分发接收task进程信息
//task执行任务