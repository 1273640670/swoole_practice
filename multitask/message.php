<?php
//1. key-只能起一个字母
$key = ftok(__DIR__, 'j');  //从当前路径创建队列
echo $key . PHP_EOL;
//2. 获取队列的资源
$quene = msg_get_queue($key);


var_dump($quene);

//主进程发消息给子进程
//类型随便给个数字
//false代表非序列化、非阻塞
msg_send($quene, 1, '主进程给你消息', false, false);

$process = new Swoole\Process(function () use ($quene) {
    //0代表获取所有消息类型
    //$msg_type代表获取消息类型
    //false代表非序列化
    msg_receive($quene, 0, $msg_type, 1024, $message, false);
    var_dump($message);
    //子进程空间执行业务逻辑
    //获取进程pid
    echo getmypid() . '正在执行任务' . PHP_EOL;
});
$process->start();  //自动进程，创建进程

//阻塞回收形式
while ($ret = Swoole\Process::wait(true)) {
//    var_dump($ret);       不能输出，否则不结束运行
}