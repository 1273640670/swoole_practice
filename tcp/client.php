<?php

////场景：硬件设备交互、消息通讯
//$client = new Swoole\Client(SWOOLE_SOCK_TCP);
//if (!$client->connect('192.168.99.100', 9501, -1)) {    //-1 防止连接超时？
//    exit("connect failed. Error: {$client->errCode}\n");
//}
//$client->send("hello world" . PHP_EOL);
//echo $client->recv() . PHP_EOL;
//$client->close();

//-----------------------------------------
//异步客户端
//场景：服务端不能及时返回数据内容
$client = new Swoole\Client(SWOOLE_SOCK_TCP, SWOOLE_SOCK_ASYNC);    //异步


//绑定事件之后才能连接
$client->on('connect', function ($client) {
    $client->send('hello');
});


$client->on('receive', function ($client, $data) {
    echo '接受消息: ' . $data . PHP_EOL;
});


$client->on('close', function ($client) {
});


$client->on('error', function ($client) {
    exit('connect failed. Error:' . $client->errCode . PHP_EOL);
});

$client->connect('192.168.99.100', 9501);    //-1 防止连接超时？