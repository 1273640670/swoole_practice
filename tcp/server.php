<?php
//默认使用提供TCP协议的服务端
$server = new Swoole\Server('0.0.0.0', 9501);

$server->set([
//    'daemonize' => true,  //后台运行，守护进程
]);

//事件驱动（连接）
$server->on('connect', function ($serv, $fd) {
    //fd是可复用的，非唯一
    echo '有新的客户端连接，连接标识为：' . $fd . PHP_EOL;
});

$server->on('receive', function ($serv, $fd, $reactor_id, $data) {
    echo '接受客户端消息：' . $data . PHP_EOL;
    $serv->send($fd, '123456');
});

$server->on('close', function ($serv, $fd, $reactor_id) {
    echo '客户端关闭，连接标识为：' . $fd . PHP_EOL;
});


$server->start();