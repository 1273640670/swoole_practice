<?php

//php version = 7.2.4
//swoole version = 4.5.2

//> 由于底层会优先执行子协程的代码，因此只有子协程挂起时，Coroutine::create 才会返回，继续执行当前协程的代码。
//go如果挂起，就会接着往下面走程序，当程序不能够往下执行，才会resume
//run模块的所有可以当成是同步中的一段代码，只有执行完该步骤后才可继续执行；run中不能再创建run
//如果sleep时间过短（如0.001毫秒），那么难以保证时间差内代码是否已经完成，同样resume的子进程顺序执行就可能存在变化


echo "main start\n";                                                        //1. 最早执行，输出main start
Co\run(function () {                                                        //2. Co\run执行完之后，才能够走下面的步骤
    //coro 1 run创建的最外层
    echo "coro " . co::getcid() . " start\n";                               //2-1. 2中最早执行，输出coro 1 start

    go(function () {
        //coro 2 go创建的coro1里面，父进程是coro 1
        echo "coro " . co::getcid() . " start\n";                           //2-2. 优先进入协程里面，但是go函数是异步的，并不是必须全部执行完成后才接着往下走，输出coro 2 start
        co::sleep(.2);                                                      //2-3. 子进程挂起后，执行当前协程的外层代码
        echo "coro " . co::getcid() . " end\n";                             //2-10（实际2-12）. 被resume执行，但是sleep 0.2秒后才执行，期间另外两个被resume的进程已经执行完毕
                                                                            //     run中所有父子进程都已执行完毕，可以顺序执行run外的代码了
    });


    go(function () {
        //coro 3 go创建的coro1里面，coro2进程已存在，那么递增为3。父进程是coro 1
        echo "coro " . co::getcid() . " start\n";                           //2-4. 优先进入协程里面，输出coro 3 start
        co::sleep(.1);                                                      //2-5. 子进程挂起后，执行当前协程的外层代码
        echo "coro " . co::getcid() . " end\n";                             //2-10. 被resume执行，但是coro 2 sleep 0.2秒，那么执行的比coro2快，且比coro1外层先执行，同样sleep 0.1，所以它会更快执行，输出coro 3 end
                                                                            //      如果sleep 0.2秒，那么下面的coro 1 sleep 0.1秒就会先执行；它跟coro 2 sleep时间一样，但是因为coro2 先执行，所以它会比coro2慢
    });

    //！不能够在run里面再使用run
    //    Co\run(function(){
    //        echo 'run '.co::getcid().' start'.PHP_EOL;
    //       co::sleep(.2);
    //       echo 'run '.co::getcid().' end'.PHP_EOL;
    //    });


    go(function () {
        //coro 4 go创建的coro1里面，coro2、3进程已存在，那么递增为4。父进程是coro 1
        echo "coro " . co::getcid() . " start\n";                           //2-6. 进入协程，输出coro 4 start
        echo "coro " . co::getcid() . " end\n";                             //2-7. 没有被挂起，直接往下执行，输出去coro 4 end
    });


    echo "coro " . co::getcid() . " do not wait children coroutine\n";      //2-8. 子进程被挂起，接着执行它,输出coro 1 do not wait children coroutine
    co::sleep(.1);                                                          //2-9. 挂起，让出当前协程，但是在run中，无法继续执行外面的命令，只能够resume父子所有进程，自己和子进程同时进行
    echo "coro " . co::getcid() . " end\n";                                 //2-10（实际2-11）. 被resume执行，但是sleep 0.1秒后才执行
});
echo "end\n";                                                               //3. run执行完后才可执行


/*
main start
coro 1 start
coro 2 start
coro 3 start
coro 4 start
coro 4 end
coro 1 do not wait children coroutine
coro 3 end
coro 1 end
coro 2 end
end
*/
